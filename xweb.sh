#!/bin/bash
startxweb( ){  
  cd /xweb/configs/
  docker-compose   up -d  $1
}


if [ "$1" = "install" ]; then  
  xweb_admin_host=$2 
  xweb_docker_moudles=$3
  ZONE=$4

  if [ -z "$xweb_admin_host"  ]; then 
     echo "How to use:   xweb.sh admin.com base "
     echo "First param：  admin host name"
     echo "Second param：  modules ,  base = openresty,php,python,ssdb  , default is base "
     echo "Third param： docker mirror's location , CH or EN , default is EN"
     exit;
  fi

  if [   "$xweb_docker_moudles" = "base" ]; then 
     xweb_docker_moudles=""
  fi

  if [ -z "$ZONE"  ]; then 
     ZONE="EN"
  fi

  #system update
  echo "system init"
  apt-get update
  yum update 
  cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
  apt-get install ntpdate git 
  yum install ntp git
  git config core.filemode false
  ntpdate pool.ntp.org
  echo "Asia/Shanghai" > /etc/timezone
  rm -rf /etc/localtime
  ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

  cd /
  #download source code
  if [ ! -f "/xweb/xweb.sh" ]; then
    echo "please download xweb first:"
    cd /
    git clone https://gitlab.com/philip2019tn/xweb.git
    echo "download  success"
  fi 


  if [ ! -f "/xweb/configs/.env" ]; then 
     cp /xweb/configs/.env.default /xweb/configs/.env
     sed -i "s/SYS_ZONE=EN/SYS_ZONE=$ZONE/g" /xweb/configs/.env 
     #sed -i "s/docker-compose-base.yml/${xweb_docker_compose_file}/g" /xweb/configs/.env
  fi 
 

  source /xweb/configs/.env
  echo $SYS_ZONE
  if ! command -v curl > /dev/null 2>&1;then
      apt install -y curl
      yum install curl
  fi 

  if command -v docker > /dev/null 2>&1;then
    echo 'exists docker' 
  else 
    curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
  fi

  if command -v docker-compose > /dev/null 2>&1;then
    echo 'exists docker-compose' 
  else 
    rm -rf /usr/local/bin/docker-compose
    rm -rf /usr/bin/docker-compose
    curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose 
    chmod +x /usr/local/bin/docker-compose 
    ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose  
  fi

  if [ ! -f "/xweb/configs/docker-compose.yml" ]; then 
      #cp /xweb/configs/${DOCKER_COMPOSE_FILE} /xweb/configs/docker-compose.yml
      cat /xweb/configs/docker-compose-base.yml > /xweb/configs/docker-compose.yml
      array=(${xweb_docker_moudles//,/ })
      for var in ${array[@]}
      do
        if [ -f "/xweb/configs/docker-compose-${var}.yml" ]; then 
          cat /xweb/configs/docker-compose-${var}.yml >> /xweb/configs/docker-compose.yml
        fi 
      done 
  fi

  if [ ! -f "/xweb/configs/nginx/xweb/server.conf" ]; then
    cp /xweb/configs/nginx/xweb/server.conf.default /xweb/configs/nginx/xweb/server.conf
    sed -i "s/admin.com/${xweb_admin_host}/g" /xweb/configs/nginx/xweb/server.conf
  fi

  if [ ! -f "/xweb/xweb/runtime/data/" ]; then
      mkdir -p /xweb/xweb/runtime/data/
  fi

  if [ ! -f "/xweb/xweb/runtime/cache/" ]; then
      mkdir -p /xweb/xweb/runtime/cache/
  fi  
  echo "Change Permission of cache"
  #chmod -vR 777 /xweb/xweb/runtime/cache/
  #rm -rvf /xweb/xweb/runtime/cache/*
  chown -vR 1100:1100 /xweb/xweb/runtime/cache/

  if [ ! -f "/xweb/xweb/runtime/backup/" ]; then
      mkdir -p /xweb/xweb/runtime/backup/
  fi 

  cd /xweb/configs/
  docker-compose   build  

  if [ ! -f "/usr/bin/xweb" ]; then  
      ln -s /xweb/xweb.sh /usr/bin/xweb
  fi
  echo 'install success.......' 
  xweb start 
  echo "host: ===================  http://$xweb_admin_host  ===================="
fi

if [ "$1" = "stop" ]; then 
  cd /xweb/configs/
  docker-compose   stop $2
  echo 'stop success.......' 
fi

if [ "$1" = "start" ]; then 
  startxweb $2
   echo 'start success.......' 
fi

if [ "$1" = "clean" ]; then 
   df -h | grep -E "^(/)"
   rm -rvf /xweb/xweb/runtime/cache/*
   docker rmi -f $(docker images | grep "<none>" | awk "{print \$3}")
   df -h | grep -E "^(/)"
   echo 'clean docker images success.......' 
fi

if [ "$1" = "estart" ]; then 
   cd /xweb/configs/
   docker-compose  -f docker-compose-ext.yml  up -d  $2
   echo 'start success.......' 
fi

if [ "$1" = "restart" ]; then 
  cd /xweb/configs/
  docker-compose   stop $2
  echo 'stop success.......'  
  startxweb $2
  echo 'start success.......' 
fi

if [ "$1" = "erestart" ]; then 
  cd /xweb/configs/
  docker-compose   -f docker-compose-ext.yml stop $2
  echo 'stop success.......'  
  #startxweb $2
  docker-compose  -f docker-compose-ext.yml  up -d  $2
  echo 'start success.......' 
fi

if [ "$1" = "build" ]; then 
  cd /xweb/configs/
  docker-compose   build --no-cache $2
  echo 'build success.......' 
fi

if [ "$1" = "reload" ]; then 
  cd /xweb/configs/
  docker exec  xweb-openresty  openresty -s reload
  echo 'reload openresty success.......' 
fi



